# Formula 1 RDF Database

 This Repository contains all the code and material used for the first team project of the Database 2 course of the Computer Engineering Master Degree at Università degli Studi di Padova.


Team Members:

- Tommaso Baldo
- Elia Detogni
- Andrea Frasson

## Brief Explanation of the Project

 The intent of the project is to develop an RDF Database about F1(Formula 1) by keeping track of all races, drivers, constructors, circuits that appeared in the World Championships from 1950 onwards.

 The starting dataset is at: https://www.kaggle.com/cjgdev/formula-1-race-data-19502017. From there we enriched it with other data useful to the project:

 - We added for most entities (for example Driver) the corresponding WikiData URL to be used as identifier in the Database;
 - For each season we added the corresponding Driver and Constructor champions;
 - We added the typology for each circuit(permanent or city-circuit) and its length;

**The majority of data enrichment was done by writing some Python Notebooks but for completeness some data had to be put by hand(holes left by the programs, wrong data ecc).**

## Content of the Repository 
```
 |
 |- Data Integration Notebook     Contains the notebooks used for the data enrichment
 |- Populate RDF Notebooks        Contains the notebook used for the serialization
 |
 |- data
     |- rdf           Contains the turtle files
	 |- countryCodes  Contains some useful csv used for the conversione Country-Nationality
 |- dataset-f1        The dataset used to populate the database
 |- Graphs            Contains all grapsh and images that will be used in the presentation of the Project
 |- Ontology          Contains all the files regarding the Ontology
 |- Queries
     |- queries           A notebook containing a selection of 10 insightful queries
	 |- other-queries     Contains some other queries we wrote in .txt format
     |- selected-queries  Contains the ten selected queries in .txt format
```

## Usage


```
 To generate the .ttl files simply run the notebok inside the "Populate RDF Notebook". They will be saved inside "data/rdf"
```

## Data expansion
To test the data expansion notebooks a copy of the dataset is inside the folder dataset-f1/raw_dataset. 
  
The notebooks mainly parse wikidata to get the URLs of entities and additional data, the outputs are written to new files that were subsequently used for the data ingestion process.  
  
The first notebook to run should be the notebook "expand-wikidata.ipynb" as it retrieves the wikidata URL used by the other notebooks to retrieve more data.  
  
Note that some data was still inserted by hand to fix things the parsers missed, for example wrong or missing URLs, wrong or incomplete data and so on. Especially the names for circuits and drivers were badly encoded and displayed incorrectly.  
  
To fix the encoding issues we retrieved and used a newer dataset, not available when we first started working on this project, presentd in the folder dataset-f1/2021. Since we started this project with the old dataset, despite a newer one becoming available, we decided to stick with the old one.








