PREFIX f1: <https://dei.unipd.it/database2/project1/formula1#>
#top 100 of drivers that gained more positions since 1970
select ?driverName ?raceInfo ?diff ?grid ?arrival where{
    ?app a f1:Appearance ;
         f1:raceIn ?race ;
         f1:gridPosition ?grid ;
         f1:arrivePosition ?arrival .
    
    ?driver f1:participateIn ?app ;
            f1:firstName ?n1 ;
            f1:lastName ?n2 .
    
    ?race f1:name ?raceName ;
          f1:date ?date .
    
    filter(year(?date) >= 1970)
    
    bind(concat(?n1, " ", ?n2) as ?driverName) .
    bind(concat(?raceName, " ", str(year(?date))) as ?raceInfo)
    bind((?grid - ?arrival) as ?diff) .
}
order by desc(?diff)
limit 100
#DA USARE