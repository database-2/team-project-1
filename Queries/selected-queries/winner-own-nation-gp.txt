PREFIX f1: <https://dei.unipd.it/database2/project1/formula1#>
#drivers ordered by name of wins in gps in their origin country
SELECT (GROUP_CONCAT(DISTINCT ?name; separator=", ") AS ?drivers) ?nationality ?wins
WHERE
{
    ?driver a f1:Driver;
            f1:firstName ?fname;
            f1:lastName ?lname;
            f1:hasNationality ?nationality
    BIND (CONCAT (?fname, " ", ?lname) AS ?name)
    {	#SUB QUERY 1: For each nation, select the max number of time a driver won his own country GP
        SELECT ?nationality (MAX(?wins) AS ?nationRecord)
        WHERE
        {
            {	#Count the number of time a driver won his own country GP
                SELECT ?driver ?nationality (COUNT(?partecipation) AS ?wins)
                WHERE{
                    ?driver a f1:Driver;
                            f1:hasNationality ?nationality;
                            f1:participateIn ?partecipation.
                    ?partecipation f1:arrivePosition ?pos;
                                   f1:raceIn ?race.
                    ?race f1:isHosted ?circuit.
                    ?circuit f1:hasCountry ?country.
                    FILTER( (?pos = 1) && (?nationality = ?country) )
                }
                GROUP BY ?driver ?nationality
            }
        }
        GROUP BY ?nationality
    }
    {	#SUB-QUERY 2: #Count the number of time a driver won his own country GP
        SELECT ?driver ?nationality (COUNT(?partecipation) AS ?wins)
        WHERE
        {
            ?driver a f1:Driver;
                    f1:hasNationality ?nationality;
                    f1:participateIn ?partecipation.
            ?partecipation f1:arrivePosition ?pos;
                           f1:raceIn ?race.
            ?race f1:isHosted ?circuit.
            ?circuit f1:hasCountry ?country.
            FILTER( (?pos = 1) && (?nationality = ?country) )
        }
        GROUP BY ?driver ?nationality
    }
    #Compare the number of time a driver won his own country GP with his country "nationRecord"
    FILTER(?wins=?nationRecord)
}
GROUP BY ?nationality ?wins
ORDER BY DESC(?wins)

#DA USARE