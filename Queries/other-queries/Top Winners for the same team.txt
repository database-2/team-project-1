#for each constructor driver that won more
PREFIX f1: <https://dei.unipd.it/database2/project1/formula1#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
select ?constructor ?driver (COUNT(?partecipation) as ?wins) where { 
	?partecipation f1:raceFor ?constructor;
                f1:arrivePosition "1"^^xsd:int.
    ?driver f1:participateIn ?partecipation.
    
}
group by ?driver ?constructor
order by desc (?wins)